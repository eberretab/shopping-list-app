package clases

import android.content.Context
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley


open class Sucursal : webservice {
    public var id_usuario = "";
     val email = null;
     public var nombre = "";
    private  val URL_CONTROLLER = "/controllers/sucursal_controller.php"
     public var latitud           = ""
     public var longitud          = ""
     var horario_lunes     = ""
     var horario_martes    = ""
     var horario_miercoles = ""
     var horario_jueves    = ""
     var horario_viernes   = ""
     var horario_sabado    = ""
     var horario_domingo   = ""
    public var icon = "";
    var direccion = ""

    constructor(){
    }

    public fun getSucursales( context: Context, callback:Response.Listener<String>) {
        val stringRequest = object : StringRequest(
            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() }}) {
            override fun getParams(): Map<String, String> { val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "getAll";
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    public fun getSucursal(id_sucursal:String, context: Context, callback:Response.Listener<String>) {
        val stringRequest = object : StringRequest(
            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() }}) {
            override fun getParams(): Map<String, String> { val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "get";
                params["id_sucursal"] = id_sucursal;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }


}
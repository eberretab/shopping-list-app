package clases

import android.content.Context
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class Lista : webservice {
    public var id_lista = "";
    public var nombre = "";
    public var id_usuario = "";
    private val URL_CONTROLLER = "/controllers/lista_controller.php"
    public lateinit var productos:  MutableList<Producto>

   constructor(){

   }

    fun getListas(id_usuario: String, context: Context, callback: Response.Listener<String>) {
        this.id_usuario = id_usuario;
        val stringRequest = object : StringRequest(

            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params["key"] = KEY;
                params["function"] = "getAll";
                params["id_usuario"] = id_usuario;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun createNewList(nombre:String,id_usuario:String, context:Context, callback: Response.Listener<String>){
        this.id_usuario = id_usuario;
        this.nombre = nombre;
        val stringRequest = object : StringRequest(

            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params["key"] = KEY;
                params["function"] = "create";
                params["id_usuario"] = id_usuario;
                params["name_list"] = nombre;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }
    public fun getList(id_lista:String, context:Context, callback: Response.Listener<String>) {
        this.id_lista= id_lista;
        val stringRequest = object : StringRequest(
            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "getList";
                params["id_lista"] = id_lista;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun getAllCatalogues(context: Context, callback: Response.Listener<String>) {
        val stringRequest = object : StringRequest(
            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "getAllCatalogues";
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun addProduct(producto: Producto,context: Context, callback: Response.Listener<String>) {
        producto.id_lista = this.id_lista;
        producto.save(context,callback);

    }
}
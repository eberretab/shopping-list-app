package clases

import android.content.Context
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class Producto : webservice {
    private val URL_CONTROLLER = "/controllers/producto_controller.php"
    public var id_producto = ""
    public var nombre      = "";
    public var precio      = "";
    public var id_lista    = "";
    public var comprado    = false;

    constructor(){

    }

    public fun getAll(id_lista: String, context: Context, callback: Response.Listener<String>) {
        val stringRequest = object : StringRequest(Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "getAll";
                params["id_lista"] = id_lista;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun updateComprado(checked: Boolean, context: Context, callback: Response.Listener<String>) {
        val stringRequest = object : StringRequest(Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "updateComprado";
                params["id_producto"] = id_producto;
                if(checked) params["value"] = "1" else params["value"] = "0";
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun updateValue(campo:String, value: String,context: Context,callback: Response.Listener<String>) {
        val stringRequest = object : StringRequest(Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"]         = KEY;
                params["function"]    = "updateValue";
                params["id_producto"] = id_producto;
                params["column"]      = campo;
                if(campo=="precio") {
                    if (value == "") {
                        params["value"] = "0";
                    } else {
                        params["value"] = value;
                    }
                }else{
                    params["value"] = value;
                }
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun getCatalogoAllSucursales(context: Context, callback: Response.Listener<String>) {
        val stringRequest = object : StringRequest(Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"]         = KEY;
                params["function"]    = "getAllCatalogs";
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }


    fun save(context: Context, callback: Response.Listener<String>) {
        val stringRequest = object : StringRequest(
            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "create";
                params["nombre"]   = nombre;
                params["precio"]   = precio;
                params["id_lista"] = id_lista;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }
}
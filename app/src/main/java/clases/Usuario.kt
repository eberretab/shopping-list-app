package clases

import android.content.Context
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley


open class Usuario : webservice {
    public var id_usuario = "";
    private val email = null;
    private val nombres = null;
    private val apellidos = null;
    private val tipo = null;
    private val URL_CONTROLLER = "/controllers/user_controller.php"

    constructor(){
    }

    public fun getUser(id_usuario: String, context: Context, callback:Response.Listener<String>) {
        this.id_usuario = id_usuario;
        val stringRequest = object : StringRequest(

            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params["key"] = KEY;
                params["function"] = "getUser";
                params["id_usuario"] = id_usuario;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    public fun registrar(nombres:String,apellidos:String,password:String){

    }

    public fun uploadImageBase64(encodedImage: String, context: Context, callback:Response.Listener<String>) {
        var id_usuario =  this.id_usuario;
        val stringRequest = object : StringRequest(

            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params["key"] = KEY;
                params["function"] = "uploadImageBase64";
                params["encodedImage"] = encodedImage;
                params["id_usuario"] = id_usuario;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun compararProductosConTiendas(lat:String, lng:String,context: Context, callback:Response.Listener<String>) {
        val stringRequest = object : StringRequest(
            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) { Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show() } }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "compararProductosConTiendas";
                params["id_usuario"] = id_usuario;
                params["lat"] = lat;
                params["lng"] = lng;
                params["rango"] = "0.300";
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }

    fun register(email:String, password: String, nombres: String, apellidos: String, context: Context, callback:Response.Listener<String>) {
        val stringRequest = object : StringRequest(
            Request.Method.POST,URL_BASE+ URL_CONTROLLER, callback,
            object : Response.ErrorListener { override fun onErrorResponse(error: VolleyError) {Toast.makeText(context, error.toString(), Toast.LENGTH_LONG).show()} }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["key"] = KEY;
                params["function"] = "register";
                params["email"]    = email;
                params["password"] = password;
                params["nombres"] = nombres;
                params["apellidos"] = apellidos;
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((context))
        requestQueue.add(stringRequest)
    }


}

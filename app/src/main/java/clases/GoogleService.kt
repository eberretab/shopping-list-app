package clases


import android.annotation.SuppressLint
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.media.RingtoneManager
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log
import android.widget.Toast
import com.android.volley.Response
import com.example.shoppinglist.R
import activities.sucursal_profile
import org.json.JSONException
import org.json.JSONObject

import java.util.Timer
import java.util.TimerTask


/**
 * Created by deepshikha on 24/11/16.
 */

class GoogleService : Service(), LocationListener {

    internal var isGPSEnable = false
    internal var isNetworkEnable = false
    internal var latitude: Double = 0.toDouble()
    internal var longitude: Double = 0.toDouble()
    internal var locationManager: LocationManager? = null
    internal var location: Location? = null
    private val mHandler = Handler()
    private var mTimer: Timer? = null
    internal var notify_interval: Long = 1000
    internal lateinit var intent: Intent

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        mTimer = Timer()
        mTimer!!.schedule(TimerTaskToGetLocation(), 500, notify_interval)
        intent = Intent(str_receiver)
        //        fn_getlocation();
    }

    override fun onLocationChanged(location: Location) {
      //  Toast.makeText(applicationContext, "from onLocationChanged (" + location.latitude.toString() + ", " + location.longitude.toString() + ") " , Toast.LENGTH_LONG).show()
        var usuario = Usuario();
        val userSettings = getSharedPreferences("userSettings", 0)
        usuario.id_usuario = userSettings.getString("id_usuario", "")
        usuario.compararProductosConTiendas(location.latitude.toString(), location.longitude.toString(), applicationContext, Response.Listener<String> { response ->

           try {

            val resp: JSONObject = JSONObject(response)
            val result = resp.getBoolean("result")
            if (result) {
                var productosJson = resp.getJSONArray("productos")
                var i = 0
                var length = productosJson.length()
                while (i < length) {
                    try {
                        var productoJson = productosJson.getJSONObject(i)
                        var producto = Producto()


                        var nombre     = productoJson.getString("nombre")
                        var sucursal   = productoJson.getString("sucursal")
                        var distancia  = productoJson.getString("distancia")
                        var id_producto= productoJson.getString("id_producto")
                        var id_sucursal= productoJson.getString("id_surcursal")

                        var mBuilder: NotificationCompat.Builder;
                        var mNotifyMgr = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager;
                        var icono = R.mipmap.ic_launcher;
                        var intent = Intent(applicationContext, sucursal_profile::class.java);
                        intent.putExtra("id_sucursal",id_sucursal);
                        var pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,intent, 0);
                        mBuilder =  NotificationCompat.Builder(applicationContext)
                            .setContentIntent(pendingIntent)
                            .setSmallIcon(icono)
                            .setContentTitle("No se te olvide comprar " + nombre)
                            .setContentText("Puedes encontrarlo en " + sucursal + " en " + distancia + " kilometros")
                            .setVibrate(longArrayOf(100, 250, 100, 500))
                            .setAutoCancel(true);
                        mNotifyMgr.notify(id_producto.toInt(), mBuilder.build());



                        try {
                            var notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            var r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                            r.play();
                        } catch (e:Exception) {
                            e.printStackTrace();
                        }
                    } catch (e: JSONException) { e.printStackTrace() }
                    i += 1
                }
            } else {
                Toast.makeText(applicationContext, resp.getString("message"), Toast.LENGTH_SHORT).show()
            }
            }catch(e:Exception){
               Toast.makeText(applicationContext, e.message.toString(),Toast.LENGTH_SHORT).show()
            }
        });

    }


    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

    }

    override fun onProviderEnabled(provider: String) {

    }

    override fun onProviderDisabled(provider: String) {

    }

    @SuppressLint("MissingPermission")
    private fun fn_getlocation() {
        locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        isGPSEnable = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
        isNetworkEnable = locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        if (!isGPSEnable && !isNetworkEnable) {

        } else {

            if (isNetworkEnable) {
                location = null
                locationManager!!.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0f, this)
                if (locationManager != null) {
                    location = locationManager!!.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                    if (location != null) {

                        Log.e("latitude", location!!.latitude.toString() + "")
                        Log.e("longitude", location!!.longitude.toString() + "")

                        latitude = location!!.latitude
                        longitude = location!!.longitude
                        fn_update(location!!)
                    }
                }

            }


            if (isGPSEnable) {
                location = null
                locationManager!!.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0f, this)
                if (locationManager != null) {
                    //location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
                    location = getLastKnownLocation();
                    if (location != null) {
                        Log.e("latitude", location!!.latitude.toString() + "")
                        Log.e("longitude", location!!.longitude.toString() + "")
                        latitude = location!!.latitude
                        longitude = location!!.longitude
                        fn_update(location!!)
                    }
                }
            }

            //Toast.makeText(applicationContext, "from fn_getlocation (" + latitude.toString() + ", " + longitude.toString() + ") " , Toast.LENGTH_LONG).show()
        }

    }

    @SuppressLint("MissingPermission")
    private fun getLastKnownLocation(): Location? {
        locationManager = applicationContext.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val providers = locationManager!!.getProviders(true)
        var bestLocation: Location? = null
        for (provider in providers) {
            val l = locationManager!!.getLastKnownLocation(provider) ?: continue
            if (bestLocation == null || l.getAccuracy() < bestLocation.accuracy) {
                // Found best last known location: %s", l);
                bestLocation = l
            }
        }
        return bestLocation
    }

    private inner class TimerTaskToGetLocation : TimerTask() {
        override fun run() {

            mHandler.post { fn_getlocation() }

        }
    }

    private fun fn_update(location: Location) {

        intent.putExtra("latutide", location.latitude.toString() + "")
        intent.putExtra("longitude", location.longitude.toString() + "")
        sendBroadcast(intent)
    }

    companion object {
        var str_receiver = "servicetutorial.service.receiver"
    }


}
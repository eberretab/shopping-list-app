package activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.shoppinglist.R
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.splash_screen.*

class splash_screen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen)
        if(estaLogueado()){
            goToHome()
        }

        btn_sing_up.setOnClickListener{
           // INICIO DE SESSION CON GOOGLE
            /*
                Este Boton es para registrarse y para iniciar sesion con google
                Si no esta registrado lo que hara sera registrarlo y guardar los datos que devuelve google en la base de datos usando el metodo register de la clase Usuario
                Si si esta registrado lo que hara sera iniciarle la session

                al iniciar la sesion debe guardar en la configuracion el id del usuario de la siguiente manera:
                        val userSettings = getSharedPreferences("userSettings",0)
                        val editor = userSettings.edit()
                        editor.putString("id_usuario",id_usuario);
                        editor.commit();
                 e iniciar el home
             */
        }
        btn_register.setOnClickListener{
            // Registro de usuarios
            /*
                1. Abrir otra actividad que sirva para ingresar los datos del nuevo usuario
                    - email
                    - contraseña
                    - Nombres
                    - Apellidos
                2. Dentro de esa actividad usar la clase Usuario. El metodo register() del a siguiente manera
                        var usuario = Usuario(email, password, nombres, apellidos,this,
                            Response.Listener<String> { response ->
                            try {
                                val resp: JSONObject = JSONObject(response) // Obtiene la respuesta del servidor
                                val result = resp.getBoolean("result") //  Obtiene el resultado
                                if (result) { //Revisa si jalo bien
                                    // Aqui hay de 2:
                                            1.- Mandarlo al splash_screen de nuevo para que inicie sesion
                                            2.- Iniciar la sesion y mandarlo al Home
                                }catch (e:JSONException){
                                    Alerter.create(this)
                                        .setText("Error:" + e.message.toString())
                                        .setTextAppearance(R.style.AlertTextWhite)
                                        .setBackgroundColorRes(com.example.shoppinglist.R.color.colorDanger)
                                        .show()
                                }
                            }
                        );
             */
        }
    }

    private fun estaLogueado(): Boolean {
        val userSettings = getSharedPreferences("userSettings", 0)
        val id_usuario = userSettings.getString("id_usuario", "")
        if(id_usuario!=""){
            return true;
        };
        return false;
    }

    fun toLogin(v: View) {
        goToLogin();
    }

    fun goToLogin(){
        val login = Intent(this, login::class.java)
        startActivity(login)
    }
    fun goToHome(){
        val home = Intent(this, home::class.java)
        startActivity(home)
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
    }
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}

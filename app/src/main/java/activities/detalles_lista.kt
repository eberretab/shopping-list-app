package activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import clases.Lista
import android.widget.Toast
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_detalles_lista.*
import org.json.JSONException
import org.json.JSONObject
import clases.Producto
import adapters.ProductoAdapter
import com.example.shoppinglist.R
import dialogs.edit_producto_dialog
import kotlinx.android.synthetic.main.content_detalles_lista.*
import java.text.NumberFormat
import java.util.*
import android.content.Intent
import android.view.View

class detalles_lista : AppCompatActivity() {
    lateinit var dialog_edit_producto : edit_producto_dialog
    lateinit var lista : Lista
    lateinit var mutablelist : MutableList<Producto>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalles_lista)
        setSupportActionBar(toolbar)
        var id_lista = intent.getStringExtra("id_lista")
        lista = Lista()
        lista.id_lista = id_lista
        fab.setOnClickListener { view ->
            val intent = Intent(this, select_item::class.java).apply {
                putExtra("id_lista", id_lista);
                putExtra("nombre_lista", lista.nombre);
            }
            startActivity(intent)
        }




        getDataOfList(id_lista)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        swipeRefresh_Layout_productos.setOnRefreshListener{
            getDataOfList(id_lista)
            onItemsLoadComplete()
        }
    }

    override fun onResume() {
        getDataOfList(lista.id_lista)
        super.onResume()
    }
    fun onItemsLoadComplete() {
        // Atualize o adapter e notifique que os dados mudaram

        // Pare o refresh animation
        swipeRefresh_Layout_productos.isRefreshing = false
    }

    fun getDataOfList(id_lista:String){
        lista.getList(id_lista, this,
            Response.Listener<String> { response ->
                val resp: JSONObject = JSONObject(response)
                val result = resp.get("id_lista")
                if (result!= null) {
                    var productosJson = resp.getJSONArray("productos")
                    var name_list = resp.getString("nombre")
                    this.title = name_list
                    lista.nombre = name_list;
                    var i = 0
                    var length = productosJson.length()
                    mutablelist = mutableListOf<Producto>()
                    var total:Double = 0.0;
                    while (i < length) {
                        try {
                            var productoJson = productosJson.getJSONObject(i)
                            var producto = Producto()
                            producto.id_producto = productoJson.getString("id_producto")
                            producto.nombre      = productoJson.getString("nombre")
                            producto.precio      = productoJson.getString("precio")
                            producto.comprado = productoJson.getString("comprado") != "0"
                            producto.id_lista = productoJson.getString("id_lista")
                            var precio = producto.precio.toDouble();
                            total += precio;
                            mutablelist.add(i, producto)
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        i += 1
                    }

                    tv_total.text = "Total: " + convertToMoney(total);
                    productos_list_view.adapter = ProductoAdapter(this, mutablelist)
                    productos_list_view.isLongClickable = true
                    productos_list_view.isClickable= true


                } else {
                    Toast.makeText(applicationContext, resp.getString("message"), Toast.LENGTH_LONG).show()
                }
            }
        );


    }

    fun mostrar_dialog_edit_producto(producto: Producto){
        dialog_edit_producto = edit_producto_dialog(producto, this)
        dialog_edit_producto.show(supportFragmentManager, "Set name of new list")

/*        dialog_edit_producto.et_precio.setOnFocusChangeListener { v, hasFocus ->
            if(!hasFocus){
                producto.updatePrecio(dialog_edit_producto.et_precio.text.toString(), this,
                    Response.Listener<String> { response ->
                        val resp: JSONObject = JSONObject(response)
                        val result = resp.getBoolean("result")
                        if (result) {

                        } else {
                            Toast.makeText(this, resp.getString("message"), Toast.LENGTH_LONG).show()
                        }
                    }
                );
            }
        }*/
       /* dialog_edit_producto.et_nombre.setOnFocusChangeListener { v, hasFocus ->
            if(!hasFocus){
                Toast.makeText(applicationContext, "Leave nombre product", Toast.LENGTH_LONG).show()
            }
        }*/

    }

    fun convertToMoney(value:Double): String? {
        val format = NumberFormat.getCurrencyInstance(Locale.US)
        return format.format(value)
    }

    fun takePhoto(v: View) {

    }
}

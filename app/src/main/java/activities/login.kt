package activities


import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import com.example.shoppinglist.R
import com.google.common.hash.Hashing
import java.nio.charset.StandardCharsets

class login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    private val URL_PHP = "/controllers/user_controller.php"
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun doLogin(v: View) {
        // Obtener datos de la interfac
        var input_email:EditText = findViewById(R.id.input_login_email);
        var input_pass:EditText  = findViewById(R.id.input_login_password);
        //var email = "eberretab@gmail.com"
        var email = input_email.text.toString();
//        var pass  =  Hashing.sha256().hashString("123123", StandardCharsets.UTF_8).toString();
        var pass  =  Hashing.sha256().hashString(input_pass.text.toString(), StandardCharsets.UTF_8).toString();
        // Realizar peticion
        val stringRequest = object : StringRequest(

            Request.Method.POST,getString(R.string.URL_BASE)+ URL_PHP ,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    val resp: JSONObject = JSONObject(response)
                    val result = resp.getBoolean("result");
                    if(result){
                        val id_usuario = resp.getString("id_usuario");
                        val nombres    = resp.getString("nombres");
                        val apellidos  = resp.getString("apellidos");
                        val email      = resp.getString("email");
                        val intent = Intent(this@login, home::class.java).apply {
                            /*
                            putExtra("user_name", id_usuario)
                            putExtra("nombres", nombres)
                            putExtra("apellidos", apellidos)
                            putExtra("email", email)
                            */
                        }
                        val userSettings = getSharedPreferences("userSettings",0)
                        val editor = userSettings.edit()
                        editor.putString("id_usuario",id_usuario);
                        editor.commit();
                        startActivity(intent)
                    }else{
                        Toast.makeText(getApplicationContext(),resp.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
        object : Response.ErrorListener {
            override fun onErrorResponse(error: VolleyError) {
                Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_LONG).show()
            }
        }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["email"] = email
                params["password"] = pass
                params["key"] = getString(R.string.key)
                params["function"] = "login";
                return params
            }
        }
        val requestQueue = Volley.newRequestQueue((getApplicationContext()))
        requestQueue.add(stringRequest)
    }
}

package activities

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.app_bar_home.*
import clases.Usuario
import android.content.*
import com.android.volley.Response
import org.json.JSONObject
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.nav_header_home.*
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.view.*
import android.widget.*
import butterknife.ButterKnife
import kotlinx.android.synthetic.main.content_home.*
import java.io.*
import java.text.SimpleDateFormat
import java.util.*
import clases.Lista
import adapters.ListaAdapter
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.preference.PreferenceManager
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import clases.GoogleService
import com.example.shoppinglist.R
import com.tapadoo.alerter.Alerter
import dialogs.set_name_new_list_dialog
import dialogs.source_image_dialog
import org.json.JSONException

class home : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {


    lateinit var dialog_image: source_image_dialog
    lateinit var dialog_new_list: set_name_new_list_dialog
    lateinit var usuario: Usuario
    lateinit var lista: Lista
    var file: File? = null
    var fileUri: Uri? = null
    var listView: ListView? = null
    val CAMERA_REQUEST_CODE = 2
    lateinit var mutablelist : MutableList<Lista>
    lateinit var imageFilePath: String
    //@BindView(R.id.imgAvatar) lateinit var imgAvatar: SimpleDraweeView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)

        setSupportActionBar(toolbar)

        // BOTON DE AGREGAR NUEVA LISTA
        new_list.setOnClickListener { view ->
            dialog_new_list = set_name_new_list_dialog()
            dialog_new_list.show(supportFragmentManager, "Set name of new list")
        }

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )

        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        // Recuperar id de usuario y consultar en la bd la info
        val userSettings = getSharedPreferences("userSettings", 0)
        val id_usuario = userSettings.getString("id_usuario", "")

        cargarListas(id_usuario)
        cargarDatosUsuario(id_usuario)


        swipeRefresh_Layout_listas.setOnRefreshListener{
            cargarListas(id_usuario)
            onItemsLoadComplete()
        }

        //startService(Intent(this, background_service::class.java));
        //// wsas
        fn_permission();
        if (boolean_permission) {
            var mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            var medit =  mPref.edit();
            var ff = mPref.getString("service", "");
            if ( "" == "") {
                medit.putString("service", "service").commit();

                intent =  Intent(getApplicationContext(),  GoogleService::class.java);
                startService(intent);

            } else {
                Toast.makeText(getApplicationContext(), "Service is already running", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please enable the gps", Toast.LENGTH_SHORT).show();
        }


    }
    fun onItemsLoadComplete() {
        // Atualize o adapter e notifique que os dados mudaram

        // Pare o refresh animation
        swipeRefresh_Layout_listas.isRefreshing = false
    }

    private fun cargarDatosUsuario(id_usuario: String) {
        usuario = Usuario()
        usuario.getUser(id_usuario, applicationContext,
            Response.Listener<String> { response ->
                val resp: JSONObject = JSONObject(response)
                val id_usuario = resp.getInt("id_usuario")
                if (id_usuario != null) {
                    val nombres = resp.getString("nombres")
                    val apellidos = resp.getString("apellidos")
                    var image = resp.getString("imagen")
                    val email = resp.getString("email")

                    // Asignar datos a la vista
                    if (image == "")
                        image = "sinfoto.jpg"


                    Picasso.get()
                        .load(getString(R.string.URL_BASE) + "/media/" + image)
                        .placeholder(R.drawable.sinfoto)
                        .error(R.drawable.sinfoto)
                        .into(menu_user_image)


                    val navigationView = findViewById<NavigationView>(R.id.nav_view)
                    val headerView = navigationView.getHeaderView(0)
                    val navUsername = headerView.findViewById(R.id.menu_user_name) as TextView
                    val navUserEmail = headerView.findViewById(R.id.menu_user_email) as TextView
                    navUsername.text = nombres + " " + apellidos
                    navUserEmail.text = email

                    menu_user_image.setOnClickListener {
                        dialog_image = source_image_dialog()
                        dialog_image.show(supportFragmentManager, "string locoshon")
                    }

                } else {
                    Toast.makeText(applicationContext, resp.getString("message"), Toast.LENGTH_LONG).show()
                }
            }
        )


    }

    @SuppressLint("ResourceAsColor")
    private fun cargarListas(id_usuario: String) {
        // LIST VIEW
        //var lista = arrayOf("Death Note", "Pokemon", "Black Jack", "Dragon Ball", "Naruto", "Neo Genesis", "Baka to Test")
        lista = Lista()
        var lista = lista.getListas(id_usuario, this,
            Response.Listener<String> { response ->
                try {
                    val resp: JSONObject = JSONObject(response)
                    val result = resp.getBoolean("result")
                    if (result) {
                        var listasJson = resp.getJSONArray("data")
                        var i = 0
                        var length = listasJson.length()
                        val listas = arrayOf<Lista>()
                        mutablelist = mutableListOf<Lista>()
                        while (i < length) {
                            try {
                                var listaJson = listasJson.getJSONObject(i)
                                var list = Lista()
                                list.id_lista = listaJson.getString("id_lista")
                                list.nombre = listaJson.getString("nombre")
                                list.id_usuario = listaJson.getString("id_usuario")
                                //listas[i] = list;
                                mutablelist.add(i, list)
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                            i += 1
                        }
                        listas_list_view.adapter = ListaAdapter(applicationContext, mutablelist)
                        listas_list_view.isLongClickable = true
                    } else {
                    }
                }catch (e:JSONException){
                    Alerter.create(this)
                        .setText("Error:" + e.message.toString())
                        .setTextAppearance(R.style.AlertTextWhite)
                        .setBackgroundColorRes(com.example.shoppinglist.R.color.colorDanger)
                        .show()
                }
            }

        )
        listas_list_view.onItemLongClickListener = AdapterView.OnItemLongClickListener{ parent, view, position, id ->
            Toast.makeText(applicationContext, "long clicked pos: " + position, Toast.LENGTH_LONG).show()
            view.setBackgroundColor(R.color.colorPrimary);
            return@OnItemLongClickListener true;
        }
        listas_list_view.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val intent = Intent(this, detalles_lista::class.java).apply {
                putExtra("id_lista", mutablelist.get(position).id_lista)
            }
            startActivity(intent)
        }
    }

    private fun encodeImage(bm: Bitmap): String {
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()

        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    private fun encodeImage(path: String): String {
        val imagefile = File(path)
        var fis: FileInputStream? = null
        try {
            fis = FileInputStream(imagefile)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        val bm = BitmapFactory.decodeStream(fis)
        val baos = ByteArrayOutputStream()
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
//Base64.de
        return Base64.encodeToString(b, Base64.DEFAULT)

    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        dialog_image.dismissMyDiag()
        var encodedImage = ""
        if (requestCode == 1 && resultCode == RESULT_OK) {
            val imageUri = data!!.data
            val imageStream = contentResolver.openInputStream(imageUri)
            val selectedImage = BitmapFactory.decodeStream(imageStream)
            encodedImage = encodeImage(selectedImage)
        } else if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {
            val imgUri = Uri.Builder()
                .scheme(file.toString())
                .path(fileUri.toString())
                .build()
            //imgAvatar.setImageURI(imgUri, this)
            //val selectedImage = this!!.loadBitmap(imgUri.toString())
            encodedImage = encodeImage(file.toString())
            // val selectedImage2 = encodeImage(fileUri.toString());
        }

        if (resultCode == RESULT_OK) {
            usuario.uploadImageBase64(encodedImage, applicationContext, Response.Listener<String> { response ->
                val resp: JSONObject = JSONObject(response)
                val result = resp.getBoolean("result")
                if (result) {
                    val image = resp.getString("image")
                    Picasso.get()
                        .load(getString(R.string.URL_BASE) + "/media/" + image)
                        .placeholder(R.drawable.sinfoto)
                        .error(R.drawable.sinfoto)
                        .into(menu_user_image)
                } else {
                    Toast.makeText(applicationContext, resp.getString("message"), Toast.LENGTH_LONG).show()
                }
            })
        }
    }

    fun pickFromGallery(v: View) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1)
    }

    fun takePhoto(v: View) {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        file = File(externalCacheDir, System.currentTimeMillis().toString() + ".jpg")
        fileUri = Uri.fromFile(file)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, CAMERA_REQUEST_CODE)
    }

    @Throws(IOException::class)
    fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName: String = "JPEG_" + timeStamp + "_"
        val storageDir: File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (!storageDir.exists()) storageDir.mkdirs()
        val imageFile = File.createTempFile(imageFileName, ".jpg", storageDir)
        imageFilePath = imageFile.absolutePath
        return imageFile
    }

    override fun onBackPressed() {
        /* if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }*/
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.Sucursales -> {
                val mapIntent = Intent(this@home, MapsSucursales::class.java).apply {}
                startActivity(mapIntent)
            }
            R.id.nav_about -> {

                val builder = AlertDialog.Builder(this)
                builder.setTitle("Acerca del proyecto")
                builder.setMessage("Aplicación realizada por los alumnos Eber Reta, Ruben Guerrero y Jonathan Garcia como proyecto final de la materia de Aplicaciones Moviles impartida por el Ing José Tadeo en el periodo Enero-Junio 2019")
                //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                }


                builder.show()

/*
                Alerter.create(this@home)
                    .setTitle("Acerca del proyecto")
                    .setText("Aplicación realizada por los alumnos Eber Reta, Ruben Guerrero y Jonathan Garcia como proyecto final de la materia de Aplicaciones Moviles impartida por el Ing José Tadeo en el periodo Enero-Junio 2019")
                    .setTitleAppearance(R.style.AlertTextAppearance)
                    .setDuration(20000)
                    .addButton("Muy bien!", R.style.AlertButton, View.OnClickListener {
                    })
                    .show()
*/
            }
            /*R.id.nav_manage -> {

            }*/
            R.id.nav_share -> {
                val sharingIntent = Intent(android.content.Intent.ACTION_SEND)
                sharingIntent.type = "text/html"
                val shareBody = "Descarga Shopping-list desde la PlayStore!"
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here")
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody)
                startActivity(Intent.createChooser(sharingIntent, "Share via"))
            }
            R.id.nav_logout -> {
                val userSettings = getSharedPreferences("userSettings", 0)
                val editor = userSettings.edit()
                editor.putString("id_usuario", "")
                editor.commit()
                val i = baseContext.packageManager.getLaunchIntentForPackage(baseContext.packageName)
                i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(i)
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun createNewList(v:View){
        var nameList = dialog_new_list.et_name_list.text.toString()
        lista.createNewList(nameList,usuario.id_usuario, applicationContext,
            Response.Listener<String> { response ->
                val resp: JSONObject = JSONObject(response)
                val result = resp.getBoolean("result")
                if (result) {
                    cargarListas(usuario.id_usuario)
                    dialog_new_list.dismissMyDiag()
                    Alerter.create(this)
                        .setText("lista añadida")
                        .setIcon(R.drawable.ic_list)
                        .setBackgroundColorRes(com.example.shoppinglist.R.color.colorPrimary)
                        .setDuration(1000)
                        .show()


                } else {
                    Alerter.create(this)
                        .setText(resp.getString("message"))
                        .setTextAppearance(R.style.AlertTextWhite)
                        .setIcon(R.drawable.ic_list)
                        .setBackgroundColorRes(com.example.shoppinglist.R.color.colorDanger)
                        .setDuration(1000)
                        .show()

                }
            }
        );
    }

    var REQUEST_PERMISSIONS: Int= 100;
    var boolean_permission: Boolean = false;
    var latitude:Double = 0.0
    var longitude:Double = 0.0
    lateinit var geocoder:Geocoder;
    private fun fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            if ((ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.ACCESS_FINE_LOCATION))) {


            } else {
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),REQUEST_PERMISSIONS);

            }
        } else {
            boolean_permission = true;
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            if(requestCode == REQUEST_PERMISSIONS) {
                if (grantResults.count() > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission = true;
                } else {
                    Toast.makeText(getApplicationContext(), "Please allow the permission", Toast.LENGTH_LONG).show();

                }
            }
    }
  var broadcastReceiver = object :  BroadcastReceiver() {
      override fun onReceive(context: Context?, intent: Intent?) {
            latitude = intent!!.getStringExtra("latutide").toDouble();
            longitude = intent.getStringExtra("longitude").toDouble();
            var addresses: List<Address>? = null;
            try {
                /*addresses = geocoder.getFromLocation(latitude, longitude, 1);
                var cityName = addresses.get(0).getAddressLine(0);
                var  stateName = addresses.get(0).getAddressLine(1);
                var countryName= addresses.get(0).getAddressLine(2);*/
                //Toast.makeText(applicationContext, "from broadcastReceiver(" + latitude.toString() + ", " + longitude.toString() + "): " , Toast.LENGTH_LONG).show()
            } catch (e1:IOException) {
                e1.printStackTrace();
            }
        }
    };


    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver, IntentFilter(GoogleService.str_receiver));
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver);
    }

}
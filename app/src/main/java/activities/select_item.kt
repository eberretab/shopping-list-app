package activities

import adapters.CatalogoAdapter
import adapters.ProductoAdapter
import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.shoppinglist.R
import kotlinx.android.synthetic.main.activity_select_item.*


import android.widget.*
import clases.Lista
import clases.Producto
import com.android.volley.Response
import kotlinx.android.synthetic.main.activity_select_item.view.*
import kotlinx.android.synthetic.main.content_detalles_lista.*
import org.json.JSONException
import org.json.JSONObject
import android.content.ClipData.Item
import android.widget.ExpandableListView
import android.view.View
import com.tapadoo.alerter.Alerter
import kotlinx.android.synthetic.main.layout_child.*


@Suppress("DEPRECATION")
class select_item : AppCompatActivity() {
    lateinit var textView:TextView
    public lateinit var adapter: CatalogoAdapter;
    lateinit var mutablelist: MutableList<Lista>
    lateinit var lista:Lista;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.shoppinglist.R.layout.activity_select_item)

        setSupportActionBar(toolbar_select_item)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        lista = Lista();
        lista.getAllCatalogues(applicationContext, Response.Listener<String> { response ->
            val resp: JSONObject = JSONObject(response)
            val result = resp.getBoolean("result")
            if (result) {
                var listasJson = resp.getJSONArray("data")
                var i = 0
                var length_listas = listasJson.length()
                mutablelist = mutableListOf<Lista>()
                while (i < length_listas) {
                    try {
                        var listaJson = listasJson.getJSONObject(i)
                        var lista = Lista()
                        lista.id_lista = listaJson.getString("id_lista")
                        lista.nombre      = listaJson.getString("nombre")
                        var productosJson = listaJson.getJSONArray("productos");
                        var j = 0
                        var length_productos = productosJson.length()
                        var mutablelist_Productos = mutableListOf<Producto>()

                        while (j < length_productos) {
                            var productoJson = productosJson.getJSONObject(i)
                            var producto = Producto();
                            producto.id_producto = productoJson.getString("id_producto");
                            producto.nombre = productoJson.getString("nombre");
                            producto.precio = productoJson.getString("precio");
                            producto.id_lista = productoJson.getString("id_lista");
                            mutablelist_Productos.add(j,producto);
                            j +=1;
                        }
                        lista.productos = mutablelist_Productos;
                        mutablelist.add(i, lista)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    i += 1
                }
                var id_lista = intent.getStringExtra("id_lista")
                var nombre_lista = intent.getStringExtra("nombre_lista")
this.title = "Añadir a " + nombre_lista;
                lv_catalogo.setAdapter(CatalogoAdapter(this,mutablelist, id_lista,lista.nombre,
                    Response.Listener<String> { response ->
                        val resp: JSONObject = JSONObject(response)
                        val result = resp.getBoolean("result")
                        if (result) {
                            Toast.makeText(this, resp.getString("message"), Toast.LENGTH_LONG).show()
                            Alerter.create(this)
                                .setText("Producto añadido")
                                .setIcon(R.drawable.ic_add_to_card)
                                .setBackgroundColorRes(com.example.shoppinglist.R.color.colorPrimary)
                                .setDuration(1000)
                                .show()
                        } else {
                            Alerter.create(this)
                                .setText(resp.getString("message"))
                                .setTextAppearance(R.style.AlertTextWhite)
                                .setIcon(R.drawable.ic_list)
                                .setBackgroundColorRes(com.example.shoppinglist.R.color.colorDanger)
                                .setDuration(1000)
                                .show()

                        }
                    }));
                lv_catalogo.isLongClickable = true
                lv_catalogo.isClickable= true


            } else {
                Toast.makeText(applicationContext, resp.getString("message"), Toast.LENGTH_LONG).show()
            }
        });
    }


    fun alerta(){
        Alerter.create(this)
            .setTitle("Exito")
            .setText("Producto añadido")
            .setIcon(R.drawable.ic_add_icon)
            .setBackgroundColorRes(com.example.shoppinglist.R.color.colorPrimary)
            .setDuration(10000)
            .show()
    }
    override fun onBackPressed() {

        super.onBackPressed()
    }

}

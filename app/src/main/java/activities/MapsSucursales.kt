package activities

import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import clases.Sucursal
import com.android.volley.Response
import com.example.shoppinglist.R
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.koushikdutta.ion.Ion
import org.json.JSONException
import org.json.JSONObject

class MapsSucursales : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    lateinit var mutablelist: MutableList<Sucursal>;
    override fun onMarkerClick(p0: Marker?) = false
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps_sucursales)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        setUpMap()

        // Add a marker in Sydney and move the camera
        /* val sydney = LatLng(-34.0, 151.0)
        map.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        map.moveCamera(CameraUpdateFactory.newLatLng(sydney))*/
        putSucursalesInMap();


    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        map.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                Toast.makeText(applicationContext,location.latitude.toString() + "," + location.longitude.toString() , Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun putSucursalesInMap() {
        var sucursal = Sucursal();
        var lista = sucursal.getSucursales(this,
            Response.Listener<String> { response ->
                val resp: JSONObject = JSONObject(response)
                val result = resp.getBoolean("result")
                if (result) {
                    var sucursalesJson = resp.getJSONArray("sucursales")
                    var i = 0
                    var length = sucursalesJson.length()
                    mutablelist = mutableListOf<Sucursal>()
                    while (i < length) {
                        try {
                            var sucursalJon = sucursalesJson.getJSONObject(i)
                            var sucursal = Sucursal()
                            sucursal.nombre = sucursalJon.getString("nombre")
                            sucursal.latitud = sucursalJon.getString("latitud")
                            sucursal.longitud = sucursalJon.getString("longitud")
                            sucursal.icon = sucursalJon.getString("icono")
                            mutablelist.add(i, sucursal)
                            createMark(sucursal.nombre, sucursal.latitud.toDouble(), sucursal.longitud.toDouble(), sucursal.icon)

                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }
                        i += 1
                        // Aqui poner los marcadores
                    }
                } else {
                    Toast.makeText(applicationContext, resp.getString("message"), Toast.LENGTH_LONG).show()
                }
            }
        )
    }

    private fun createMark(title: String, lat: Double, lng: Double, icon:String) {
        val myPlace = LatLng(lat, lng)

        var bmImg = Ion.with(this)
            .load(getString(R.string.URL_BASE) + "/media/" + icon).asBitmap().get()
        bmImg = Bitmap.createScaledBitmap(bmImg, 65, 65, false)
        var locationMarker = map.addMarker(MarkerOptions()
            .position(myPlace)
            .title(title)
            .icon(BitmapDescriptorFactory.fromBitmap(bmImg)))
        //map.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 15.0f))
        //map.getUiSettings().setZoomControlsEnabled(true)
        //map.setOnMarkerClickListener(this)
    }

    private fun placeMarkerOnMap(location: LatLng) {
        val markerOptions = MarkerOptions().position(location)
        //map.addMarker(markerOptions)
        val myPlace = location
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 15.0f))


    }

}

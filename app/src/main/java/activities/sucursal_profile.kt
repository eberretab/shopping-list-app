package activities

import adapters.SucursalAdapter
import android.content.pm.PackageManager
import android.location.Location
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.widget.Toast
import clases.Sucursal
import com.android.volley.Response
import com.example.shoppinglist.R
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detalles_lista.*
import kotlinx.android.synthetic.main.activity_sucursal_profile.*
import kotlinx.android.synthetic.main.content_detalles_lista.*
import org.json.JSONObject
import android.content.Intent

import android.net.Uri


class sucursal_profile : AppCompatActivity(), OnMapReadyCallback {
    lateinit var sucursal:Sucursal
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.shoppinglist.R.layout.activity_sucursal_profile)
        setSupportActionBar(toolbar_sucursal)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(com.example.shoppinglist.R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        swipeRefresh_Sucursal.setOnRefreshListener{
            getData()
            onItemsLoadComplete()
        }
    }
    fun onItemsLoadComplete() {
        // Atualize o adapter e notifique que os dados mudaram

        // Pare o refresh animation
        swipeRefresh_Sucursal.isRefreshing = false
    }
    fun getData(){
        var id_sucursal = intent.getStringExtra("id_sucursal");
        sucursal = Sucursal();
        sucursal.getSucursal(id_sucursal, applicationContext,
            Response.Listener<String> { response ->
                val resp: JSONObject = JSONObject(response)
                val result = resp.getString("id_sucursal")
                if (result != "") {
                    sucursal.nombre = resp.getString("nombre");
                    profile_sucursal_name.text = sucursal.nombre;

                    sucursal.latitud = resp.getString("latitud");
                    sucursal.longitud = resp.getString("longitud");

                    val location = LatLng(sucursal.latitud.toDouble(), sucursal.longitud.toDouble())
                    map.addMarker(MarkerOptions().position(location).title(sucursal.nombre))
                    map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15.0f))

                    profile_sucursal_tel.text = resp.getString("telefono");
                    profile_sucursal_address.text = resp.getString("direccion");
profile_sucursal_tel.setOnClickListener {
    val callIntent = Intent(Intent.ACTION_CALL)
    callIntent.data = Uri.parse("tel:" + profile_sucursal_tel.text.trim())//change the number
    startActivity(callIntent)
}
                    sucursal.icon  = resp.getString("icono")
                    Picasso.get()
                        .load(getString(com.example.shoppinglist.R.string.URL_BASE) + "/media/" + sucursal.icon)
                        .placeholder(com.example.shoppinglist.R.drawable.sinfoto)
                        .error(com.example.shoppinglist.R.drawable.sinfoto)
                        .into(profile_sucursal_image)


                    var listaDias = mutableListOf<Horario>();
                    listaDias.add(Horario("Lunes",resp.getString("horario_lunes")))
                    listaDias.add(Horario("Martes",resp.getString("horario_martes")))
                    listaDias.add(Horario("Miercoles",resp.getString("horario_miercoles")))
                    listaDias.add(Horario("Jueves",resp.getString("horario_jueves")))
                    listaDias.add(Horario("Viernes",resp.getString("horario_viernes")))
                    listaDias.add(Horario("Sabado",resp.getString("horario_sabado")))
                    listaDias.add(Horario("Domingo",resp.getString("horario_domingo")))

                    lv_sucursal_horario.adapter = SucursalAdapter(applicationContext,listaDias)

                } else {
                    Toast.makeText(applicationContext, resp.getString("message"), Toast.LENGTH_LONG).show()
                }
            }
        )

    }




    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        setUpMap()
        getData();
    }


    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                sucursal_profile.LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

        map.isMyLocationEnabled = true

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                //placeMarkerOnMap(currentLatLng)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                //Toast.makeText(applicationContext,location.latitude.toString() + "," + location.longitude.toString() , Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun placeMarkerOnMap(location: LatLng) {
        val markerOptions = MarkerOptions().position(location)
        //map.addMarker(markerOptions)
        val myPlace = location
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(myPlace, 15.0f))


    }
}
class Horario(name_dia:String, horario :String){
    public var name_dia= name_dia;
    public var horario= horario;

}
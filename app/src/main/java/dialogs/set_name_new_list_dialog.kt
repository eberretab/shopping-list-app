package dialogs


import android.app.AlertDialog
import android.app.Dialog
import android.support.v4.app.DialogFragment
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.EditText
import com.example.shoppinglist.R


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class set_name_new_list_dialog : DialogFragment() {

    public lateinit var et_name_list : EditText;

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val myView = inflater.inflate(R.layout.fragment_set_name_new_list_dialog, null);
        et_name_list= myView!!.findViewById<EditText>(R.id.et_name_new_list)
        builder.setView(myView )
        // Create the AlertDialog object and return it

        return builder.create()

    }

    public fun dismissMyDiag() {
        dialog.dismiss()
    }


}

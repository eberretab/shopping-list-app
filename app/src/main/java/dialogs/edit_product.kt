package dialogs
import activities.detalles_lista
import adapters.ListaAdapter
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText

import com.example.shoppinglist.R
import android.support.v4.content.ContextCompat.getSystemService
import android.widget.TextView
import android.widget.Toast
import clases.Lista
import clases.Producto
import com.android.volley.Response
import kotlinx.android.synthetic.main.content_home.*
import org.json.JSONException
import org.json.JSONObject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
@SuppressLint("ValidFragment")
class edit_producto_dialog(producto: Producto, context: Context) : DialogFragment() {
    public lateinit var et_precio :EditText;
    public lateinit var et_nombre :EditText;
    public var producto = producto;
    public var contexto = context;
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val myView = inflater.inflate(R.layout.fragment_create_new_producto, null);
        //Recuperar los elementos del layout

        et_precio =  myView!!.findViewById<EditText>(R.id.et_new_producto_precio )
        et_nombre =  myView!!.findViewById<EditText>(R.id.et_new_producto_nombre)
        et_precio.setText (producto.precio)
        et_nombre.setText(producto.nombre)
        et_precio.setOnFocusChangeListener { v, hasFocus ->
            if(!hasFocus){
                updateValue("precio",et_precio.text.toString());
            }
        }
        et_nombre.setOnFocusChangeListener { v, hasFocus ->
             if(!hasFocus){
                 updateValue("nombre",et_nombre.text.toString());

             }
         }





        // et_precio_producto= myView!!.findViewById<EditText>(R.id.et_name_new_list)
        builder.setView(myView)
        // Create the AlertDialog object and return it
        return builder.create()

    }


    override fun onDismiss(dialog: DialogInterface?) {
        updateValue("precio",et_precio.text.toString());
        updateValue("nombre",et_nombre.text.toString());
        super.onDismiss(dialog)
        (this.contexto as detalles_lista).getDataOfList(producto.id_lista)
    }
    public fun dismissMyDiag() {
        dialog.dismiss()
    }

    private fun updateValue(campo:String, value:String) {
        producto.updateValue(campo,value, contexto ,
            Response.Listener<String> { response ->
                print(response);
                val resp: JSONObject = JSONObject(response)
                val result = resp.getBoolean("result")
                if (result) {

                } else {
                    Toast.makeText(contexto , resp.getString("message"), Toast.LENGTH_LONG).show()
                }
            }
        );
    }


}

package dialogs


import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.app.AlertDialog
import android.app.Dialog
import com.example.shoppinglist.R


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class source_image_dialog : DialogFragment() {
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater


        builder.setView(inflater.inflate(R.layout.fragment_source_image_dialog, null))

        // Create the AlertDialog object and return it
        return builder.create()

    }

    public fun dismissMyDiag() {
        dialog.dismiss()
    }


}

package adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import clases.Lista
import com.example.shoppinglist.R

class ListaAdapter(context: Context, Listas: MutableList<Lista>) : BaseAdapter() {
    private lateinit var listas:MutableList<Lista>;
    private val mInflator: LayoutInflater;

    init {
        this.mInflator = LayoutInflater.from(context)
        listas = Listas;
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val vh: ListRowHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.list_row, parent, false)
            vh = ListRowHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ListRowHolder
        }

        vh.txview.text = listas[position].nombre
        return view
    }

    override fun getCount(): Int { return listas.size }
    override fun getItem(position: Int): Any { return listas[position] }
    override fun getItemId(position: Int): Long { return position.toLong() }
}

class ListRowHolder(row: View?) {
    public val txview: TextView

    init {
        this.txview = row?.findViewById(R.id.text_lista) as TextView
    }
}

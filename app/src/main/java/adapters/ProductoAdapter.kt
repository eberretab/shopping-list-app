package adapters

import activities.detalles_lista
import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import clases.Producto
import clases.Lista
import com.android.volley.Response
import com.example.shoppinglist.R
import dialogs.edit_producto_dialog
import kotlinx.android.synthetic.main.content_home.*
import org.json.JSONException
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*

class ProductoAdapter(context: Context, Productos: MutableList<Producto>) : BaseAdapter(), Filterable {
    override public fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
                var mPois = filterResults.values as List<Producto>
                notifyDataSetChanged()
            }

            override fun performFiltering(charSequence: CharSequence?): Filter.FilterResults {
                val queryString = charSequence?.toString()

                val filterResults = Filter.FilterResults()
                filterResults.values = if (queryString==null || queryString.isEmpty())
                    productos
                else
                    productos.filter {
                        it.nombre.contains(queryString)
                    }
                return filterResults
            }
        }

    }

    private lateinit var productos:MutableList<Producto>;
    private var contexto =context;
    private val mInflator: LayoutInflater;

    init {
        this.mInflator = LayoutInflater.from(context)
        productos = Productos;
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val vh: ProductRowHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.product_row, parent, false)
            vh = ProductRowHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as ProductRowHolder
        }

        vh.tv_nombre.text = productos[position].nombre
        val format = NumberFormat.getCurrencyInstance(Locale.US)
        val currency = format.format(productos[position].precio.toDouble())
        println("Currency in Canada : $currency")
        vh.tv_precio.text = currency
        vh.cb_comprado.isChecked= productos[position].comprado;

        vh.cb_comprado.setOnCheckedChangeListener { buttonView, isChecked ->
            productos[position].updateComprado(isChecked,this.contexto,
                Response.Listener<String> { response -> val resp: JSONObject = JSONObject(response)
                    val result = resp.getBoolean("result")
                    if (!result) { Toast.makeText(this.contexto, resp.getString("message"), Toast.LENGTH_LONG).show() }
                }
            )
        }

        vh.tv_nombre.setOnClickListener {
            (contexto as detalles_lista).mostrar_dialog_edit_producto(productos[position]);
        }
        return view
    }

    override fun getCount(): Int { return productos.size }
    override fun getItem(position: Int): Any { return productos[position] }
    override fun getItemId(position: Int): Long { return position.toLong() }
}

class ProductRowHolder(row: View?) {
    public val tv_nombre: TextView
    public val tv_precio: TextView
    public val cb_comprado: CheckBox

    init {
        this.tv_nombre = row?.findViewById(R.id.tv_nombre) as TextView
        this.tv_precio = row?.findViewById(R.id.tv_precio) as TextView
        this.cb_comprado = row?.findViewById(R.id.cb_comprado) as CheckBox

    }
}

package adapters

import activities.Horario
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import clases.Lista
import com.example.shoppinglist.R

class SucursalAdapter(context: Context, Horarios: MutableList<Horario>) : BaseAdapter() {
    private var horarios= Horarios
    private val mInflator: LayoutInflater;

    init {
        this.mInflator = LayoutInflater.from(context)
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val view: View?
        val vh: SucursalRowHolder
        if (convertView == null) {
            view = this.mInflator.inflate(R.layout.row_horario, parent, false)
            vh = SucursalRowHolder(view)
            view.tag = vh
        } else {
            view = convertView
            vh = view.tag as SucursalRowHolder
        }

        vh.name_dia.text = horarios[position].name_dia
        vh.horario_dia.text = horarios[position].horario
        return view
    }

    override fun getCount(): Int { return horarios.size }
    override fun getItem(position: Int): Any { return horarios[position] }
    override fun getItemId(position: Int): Long { return position.toLong() }
}

class SucursalRowHolder(row: View?) {
    public val name_dia: TextView
    public val horario_dia: TextView

    init {
        this.name_dia = row?.findViewById(R.id.name_dia) as TextView
        this.horario_dia = row?.findViewById(R.id.horario_dia) as TextView
    }
}
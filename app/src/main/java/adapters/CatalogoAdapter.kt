package adapters

import activities.select_item
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import android.widget.Toast
import clases.Lista
import clases.Producto
import com.android.volley.Response
import com.example.shoppinglist.R
import com.tapadoo.alerter.Alerter
import org.json.JSONObject
import java.text.NumberFormat
import java.util.*



class CatalogoAdapter(context: Context, listas: MutableList<Lista>, id_lista:String, list_name:String,callback: Response.Listener<String>) : BaseExpandableListAdapter() {
    private var listas = listas;
    private var id_lista = id_lista;
    private var list_name = list_name;
    private var callback = callback;


    var context = context;

    override fun getGroup(groupPosition: Int): Lista {
        return listas[groupPosition];
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true;
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var convertView = convertView
        if(convertView== null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(com.example.shoppinglist.R.layout.layout_group, null)
        }
        val title = convertView?.findViewById<TextView>(com.example.shoppinglist.R.id.tv_title)
        var group:Lista = getGroup(groupPosition);
        title?.text =group.nombre;

       /* title?.setOnClickListener{
            Toast.makeText(this.context,getGroup(groupPosition).nombre,Toast.LENGTH_LONG).show()
        }*/
        return  convertView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return listas[groupPosition].productos.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Producto {
        return listas[groupPosition].productos[childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int,childPosition: Int,isLastChild: Boolean,convertView: View?,parent: ViewGroup?): View? {
        var convertView = convertView
        if(convertView== null){
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(com.example.shoppinglist.R.layout.layout_child, null)
        }
        val title = convertView?.findViewById<TextView>(com.example.shoppinglist.R.id.tv_title)
        val precio = convertView?.findViewById<TextView>(com.example.shoppinglist.R.id.tv_precio)
        var child = getChild(groupPosition,childPosition)
        title?.text = child.nombre;
        precio?.text = convertToMoney( child.precio.toDouble());

        title?.setOnClickListener{
            //Toast.makeText(this.context,getChild(groupPosition,childPosition).nombre,Toast.LENGTH_LONG).show()
            var list = Lista();
            list.id_lista = this.id_lista;
            list.addProduct(child, context,callback)
        }
        return  convertView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return listas.size;
    }

    fun convertToMoney(value:Double): String? {
        val format = NumberFormat.getCurrencyInstance(Locale.US)
        return format.format(value)
    }
}
